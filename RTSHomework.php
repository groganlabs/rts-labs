/**
    A class with 
*/
class Homework {
    public function aboveBelow($intArray, $comparison) {
        $returnVal = array("above" => 0, "below" => 0);
        for($ii = 0; $ii < count(intArray); $ii++) {
            if($intArray[$ii] > $comparison) {
                $returnVal['above']++;
            }
            // The assignment didn't say anything about values equal
            // to the comparison value, so they don't get counted
            else if($intArray[$ii] < $comparison) {
                $returnVal['below']++;
            }
        }

        return $returnVal;
    }

    public function stringRotation($originalString, $rotateBy) {
        $newString = substr($originalString, $rotateBy * -1);
        $newString .= substr($originalString, 0, $rotateBy * -1);

        return $newString;
    }
}